<?php

require_once('funzioni.php');

$path = 'lug/opml.xml';
$data = retrieveContents($path, 30);
formatPage($path, $data, 'lug/contents.html');
formatFeed($data, 'Planet LUG', 'lug/rss.xml');

$path = 'ils/opml.xml';
$data = retrieveContents($path, 30);
formatPage($path, $data, 'ils/contents.html');
formatFeed($data, 'Planet ILS', 'ils/rss.xml');

$path = 'lug/calendars.txt';
$data = retrieveCalContents($path, 30);
formatCalPage($path, $data, 'lug/events.html');

