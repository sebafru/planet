<?php

require_once('vendor/autoload.php');
require_once('config.php');

use FeedIo\Factory;
use Innoscience\ComradeOPML\ComradeOPML;

use CalDAVClient\Facade\CalDavClient;
use CalDAVClient\Facade\Requests\CalendarQueryFilter;
use Sabre\VObject;

function lugheader ($title, $tagline) { ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="it">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="italian" />
	<meta name="robots" content="noarchive" />
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link href="https://www.linux.it/shared/index.php?f=bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="https://www.linux.it/shared/index.php?f=main.css" rel="stylesheet" type="text/css" />

	<meta name="dcterms.creator" content="Italian Linux Society" />
	<meta name="dcterms.type" content="Text" />
	<link rel="publisher" href="http://www.ils.org/" />

	<meta name="twitter:title" content="<?php echo $tagline ?>" />
	<meta name="twitter:creator" content="@ItaLinuxSociety" />
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:url" content="http://planet.linux.it/" />
	<meta name="twitter:image" content="http://planet.linux.it/images/tw.png" />

	<meta property="og:site_name" content="<?php echo $title ?>" />
	<meta property="og:title" content="<?php echo $title ?>" />
	<meta property="og:url" content="http://planet.linux.it/" />
	<meta property="og:image" content="http://planet.linux.it/images/fb.png" />
	<meta property="og:type" content="website" />
	<meta property="og:country-name" content="Italy" />
	<meta property="og:email" content="webmaster@linux.it" />
	<meta property="og:locale" content="it_IT" />
	<meta property="og:description" content="<?php echo $tagline ?>" />

	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.3/jquery.min.js"></script>

	<title><?php echo $title ?></title>

	<link rel="alternate" type="application/rss+xml" title="Planet LUG" href="https://planet.linux.it/lug/rss.xml" />
	<link rel="alternate" type="application/rss+xml" title="Planet ILS" href="https://planet.linux.it/ils/rss.xml" />
</head>

<body>

<div id="header">
	<img src="/images/logo.png" alt="<?php echo $title ?>" />
	<div id="maintitle"><?php echo $title ?></div>
	<div id="payoff"><?php echo $tagline ?></div>

	<div class="menu">
		<a class="generalink" href="/lug/">Planet LUG</a>
		<a class="generalink" href="/ils/">Planet ILS</a>
		<a class="generalink" href="/contatti/">Contatti</a>

		<p class="social mt-2">
			<a href="https://twitter.com/ItaLinuxSociety"><img src="https://www.linux.it/shared/index.php?f=immagini/twitter.png"></a>
			<a href="https://www.facebook.com/ItaLinuxSociety/"><img src="https://www.linux.it/shared/index.php?f=immagini/facebook.png"></a>
			<a href="https://gitlab.com/ItalianLinuxSociety/Planet"><img src="https://www.linux.it/shared/index.php?f=immagini/gitlab.png"></a>
		</p>
	</div>
</div>

<br>

<?php
}

function lugfooter () {
?>

<div id="ils_footer" class="mt-5">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<span style="text-align: center; display: block">
					<a href="http://www.gnu.org/licenses/agpl-3.0-standalone.html" rel="license">
						<img src="https://www.linux.it/shared/index.php?f=immagini/agpl3.svg" style="border-width:0" alt="AGPLv3 License">
					</a>

					<a href="http://creativecommons.org/publicdomain/zero/1.0/deed.en_US" rel="license">
						<img src="https://www.linux.it/shared/index.php?f=immagini/cczero.png" style="border-width:0" alt="Creative Commons License">
					</a>
				</span>
			</div>

			<div class="col-md-3">
				<h2>RESTA AGGIORNATO!</h2>
				<script type="text/javascript" src="https://www.linux.it/external/widgetnewsletter.js"></script>
				<div id="widgetnewsletter"></div>
			</div>

			<div class="col-md-3">
				<h2>Amici</h2>
				<p style="text-align: center">
					<a href="https://www.ils.org/info#aderenti">
						<img src="https://www.ils.org/sites/ils.org/files/associazioni/getrand.php" border="0" /><br />
						Scopri tutte le associazioni che hanno aderito a ILS.
					</a>
				</p>
			</div>

			<div class="col-md-3">
				<h2>Network</h2>
				<script type="text/javaScript" src="https://www.linux.it/external/widgetils.php?referrer=donazioni"></script>
				<div id="widgetils"></div>
			</div>
		</div>
	</div>

	<div style="clear: both"></div>
</div>

<!-- Piwik -->
<script type="text/javascript">
	var _paq = _paq || [];
	_paq.push(['disableCookies']);
	_paq.push(['trackPageView']);
	_paq.push(['enableLinkTracking']);
	(function() {
		var u="//pergamena.lugbs.linux.it/";
		_paq.push(['setTrackerUrl', u+'piwik.php']);
		_paq.push(['setSiteId', 20]);
		var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
		g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
	})();
</script>
<noscript><p><img src="//pergamena.lugbs.linux.it/piwik.php?idsite=20" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

</body>
</html>

<?php
}

function retrieveContents($filepath, $limit)
{
	$feedIo = Factory::create()->getFeedIo();
	$document = ComradeOPML::importFile($filepath);
	$items = [];

	foreach ($document->getAllCategories() as $category) {
		foreach ($category->getAllFeeds() as $feed) {
			try {
				$sitename = $feed->getText();

				$url = $feed->getXmlUrl();
				$result = $feedIo->read($url);
				foreach ($result->getFeed() as $item) {
					$item->setTitle(sprintf('%s - %s', $sitename, $item->getTitle()));
					$items[] = $item;
				}
			}
			catch(\Exception $e) {
				echo "Impossibile leggere il feed di " . $feed->getTitle() . ": " . $e->getMessage() . "\n";
			}
		}
	}

	$items = array_filter($items, function($i) {
		return !is_null($i->getLastModified()) && !empty($i->getTitle()) && !empty($i->getDescription());
	});

	usort($items, function($first, $second) {
		return $first->getLastModified()->format('Y-m-d') <=> $second->getLastModified()->format('Y-m-d');
	});

	return array_slice(array_reverse($items), 0, $limit);
}

function formatPage($opml, $data, $outfile)
{
	$sorted = [];

	foreach($data as $i) {
		$date = $i->getLastModified()->format('d/m/y');

		if (!isset($sorted[$date])) {
			$sorted[$date] = [];
		}

		$sorted[$date][] = (object) [
			'title' => $i->getTitle(),
			'link' => $i->getLink(),
			'description' => $i->getDescription(),
		];
	}

	$data = $sorted;

	$html = '';

	$html .= '<div class="row">';

	$html .= '<div class="col-8" style="overflow-x: hidden">';

	foreach($data as $date => $feeds) {
		$html .= '<div class="intro"><h2>' . $date . '</h2></div><hr>';

		foreach($feeds as $feed) {
			$html .= '<h4><a href="' . $feed->link . '">' . $feed->title . '</a></h4>';
			$html .= '<div>' . $feed->description . '</div><hr>';
		}
	}

	$html .= '</div>';

	$html .= '<div class="col-4">';
	$document = ComradeOPML::importFile($opml);

	$html .= '<a href="rss.xml"><img src="/images/feed-icon.png" alt="Planet"> Tutti i feed</a><br><br>';

	foreach ($document->getAllCategories() as $category) {
		foreach ($category->getAllFeeds() as $feed) {
			$url = $feed->getHtmlUrl();
			$name = $feed->getText();
			$html .= '<a href="' . $url . '"><img src="/images/feed-icon.png" alt="Feed ' . $name . '"> ' . $name . '</a><br>';
		}
	}

	$html .= '<br><br>I feed qui aggregati vengono automaticamente estratti dai siti indicizzati sulla <a href="http://lugmap.linux.it/">LugMap</a>';
	$html .= '</div>';

	$html .= '</div>';

	file_put_contents($outfile, $html);
}

function formatFeed($data, $title, $outfile)
{
	try {
		$feed = new FeedIo\Feed();
		$feed->addNS('wfw', 'http://wellformedweb.org/CommentAPI/');
		$feed->addNS('content', 'http://purl.org/rss/1.0/modules/content/');
		$feed->addNS('slash', 'http://purl.org/rss/1.0/modules/slash/');
		$feed->setTitle($title);

		foreach($data as $i) {
			$link = $i->getLink();
			if (strpos($link, '&') !== false) {
				$i->setLink(substr($link, 0, strpos($link, '&')));
			}

			if ($i->getAuthor() && $i->getAuthor()->getName() == null) {
				$i->setAuthor(null);
			}

			$i->setPublicId(urlencode($i->getPublicId()));
			$feed->add($i);
		}

		$feedIo = Factory::create()->getFeedIo();
		$xml = $feedIo->toRss($feed);
	}
	catch(\Exception $e) {
		echo "Impossibile formattare feed $title: " . $e->getMessage() . "\n";
		$xml = '';
	}

	file_put_contents($outfile, $xml);
}

/***********************************************************************
	Generazione fonti RSS
*/

function discoverFeed($site)
{
	static $exceptions = [];

	if (empty($exceptions) && file_exists('eccezioni.txt')) {
		$exceptions_file = file('eccezioni.txt', FILE_IGNORE_NEW_LINES);
		if ($exceptions_file != false) {
			foreach ($exceptions_file as $ex) {
				if ($ex [0] == '#')
					continue;
				$exceptions[] = trim($ex);
			}
		}
	}

	$feedIo = Factory::create()->getFeedIo();
	$feeds = $feedIo->discover($site);

	foreach ($feeds as $feed) {
		if (in_array($feed, $exceptions)) {
			continue;
		}

		if (strncmp($feed, 'http', 4) != 0) {
			if (substr($feed, 0, 1) != '/') {
				$feed = '/' . $feed;
			}

			$parts = parse_url($site);
			$feed = sprintf('%s://%s%s', $parts['scheme'], $parts['host'], $feed);
		}

		return $feed;
	}

	return null;
}

function retrieveFeeds()
{
	$elenco_regioni = [
		"abruzzo"    => "Abruzzo",
		"basilicata" => "Basilicata",
		"calabria"   => "Calabria",
		"campania"   => "Campania",
		"emilia"     => "Emilia Romagna",
		"friuli"     => "Friuli Venezia Giulia",
		"lazio"      => "Lazio",
		"liguria"    => "Liguria",
		"lombardia"  => "Lombardia",
		"marche"     => "Marche",
		"molise"     => "Molise",
		"piemonte"   => "Piemonte",
		"puglia"     => "Puglia",
		"sardegna"   => "Sardegna",
		"sicilia"    => "Sicilia",
		"toscana"    => "Toscana",
		"trentino"   => "Trentino Alto Adige",
		"umbria"     => "Umbria",
		"valle"      => "Valle d'Aosta",
		"veneto"     => "Veneto",
		"Italia"     => "Italia"
	];

	$ret = [];

	foreach ($elenco_regioni as $region => $name) {
		$lugs = file ('https://raw.github.com/Gelma/LugMap/master/db/' . $region . '.txt', FILE_IGNORE_NEW_LINES);

		foreach ($lugs as $lug) {
			try {
				list ($prov, $name, $zone, $site) = explode ('|', $lug);
				$feed = discoverFeed($site);

				if ($feed) {
					$ret[$name] = (object) [
						'xml' => $feed,
						'web' => $site,
					];
				}
			}
			catch(\Exception $e) {
				echo "Impossibile recuperare feed per " . $site . ": " . $e->getMessage() . "\n";
				continue;
			}
		}
	}

	ksort($ret);

	return $ret;
}

function generateLugOpml($filepath)
{
	$document = ComradeOPML::newDocument();

	foreach (retrieveFeeds() as $name => $url) {
		$document->addFeed('Main', $name, $url->xml, $name, $url->web);
	}

	ComradeOPML::exportFile($document, $filepath);
}

function generateIlsOpml($filepath)
{
	$contents = file_get_contents('https://ilsmanager.linux.it/ng/api/websites?token=' . urlencode(ILSMANAGER_TOKEN));

	$document = ComradeOPML::newDocument();

	foreach (explode("\n", $contents) as $row) {
		try {
			$row = trim($row);
			if (empty($row)) {
				continue;
			}

			list($name, $site) = explode(',', $row);

			$feed = discoverFeed($site);
			if ($feed) {
				$document->addFeed('Main', $name, $feed, $name, $site);
			}
		}
		catch(\Exception $e) {
			echo "Impossibile recuperare feed per " . $site . ": " . $e->getMessage() . "\n";
		}
	}

	ComradeOPML::exportFile($document, $filepath);
}

/***********************************************************************
	Gestione calendario
*/

function retrieveCalContents($filepath, $limit)
{
	$today = date('Ymd');
	$two_months = date('Ymd', strtotime('+2 months'));

	$file = fopen($filepath, 'r');
	$items = [];

	while($row = fgetcsv($file)) {
		try {
			switch ($row[1]) {
				case 'ical':
					$contents = file_get_contents($row[2]);
					$events = VObject\Reader::read($contents, VObject\Reader::OPTION_FORGIVING)->getBaseComponents('VEVENT');
					break;

				case 'caldav':
					$events = [];
					$caldav = new CalDavClient($row[2]);
					$filter = new CalendarQueryFilter(true, true, new DateTime());
					$res  = $caldav->getEventsByQuery($row[2], $filter);
					foreach($res->getResponses() as $event) {
						$events[] = VObject\Reader::read($event->getVCard(), VObject\Reader::OPTION_FORGIVING)->getBaseComponents('VEVENT')[0];
					}
					break;
			}

			foreach($events as $event) {
				if ($event->dtstart < $today || $event->dtstart > $two_months) {
					continue;
				}

				$event->categories = $row[3];
				$items[] = $event;
			}
		}
		catch(\Exception $e) {
			echo "Impossibile leggere il file ical di " . $row[0] . ": " . $e->getMessage() . "\n";
		}
	}

	fclose($file);

	usort($items, function($first, $second) {
		return (string) $first->dtstart <=> (string) $second->dtstart;
	});

	return array_slice(array_reverse($items), 0, $limit);
}

function formatCalPage($path, $data, $outfile)
{
	$sorted = [];

	foreach($data as $i) {
		$date = substr((string) $i->dtstart, 0, 8);

		if (!isset($sorted[$date])) {
			$sorted[$date] = [];
		}

		$sorted[$date][] = $i;
	}

	ksort($sorted);
        $data = $sorted;

	$html = '';

	$html .= '<div class="row">';

	$html .= '<div class="col-8" style="overflow-x: hidden">';

	foreach($data as $date => $events) {
		$formatted_date = join('/', [substr($date, -2), substr($date, -4, 2), substr($date, 0, 4)]);
		$html .= '<div class="intro"><h2>' . $formatted_date . '</h2></div><hr>';

		foreach($events as $event) {
			$html .= '<h4>' . $event->categories . ' - ' . $event->summary . '</h4>';
			if (!empty($event->location))
				$html .= '<div>' . $event->description . '<br><br>' . $event->location . '</div><hr>';
			else
				$html .= '<div>' . $event->description . '</div><hr>';
		}
	}

	$html .= '</div>';

	$html .= '<div class="col-4">';

	$file = fopen($path, 'r');
	while($row = fgetcsv($file)) {
		$html .= $row[0] . '<br>';
	}

	$html .= '</div>';

	$html .= '</div>';

	file_put_contents($outfile, $html);
}

