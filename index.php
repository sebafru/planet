<?php

require_once ('funzioni.php');
lugheader ('Italian Linux Planet', 'News dal mondo freesoftware in Italia');

?>

<div class="container main-contents">
	<div class="row">
		<div class="col">
			<p>
				Italian Linux Planet: la tua fonte di informazione su quel che accade nella community freesoftware italiana!
			</p>
			<p>
				Qui trovi gli aggregatori di <a href="https://www.ils.org/">Italian Linux Society</a>, in cui sono raccolti i contenuti prelevati da svariati siti nella blogosfera nostrana che parlano di tecnologia, attualità, ma soprattutto di software libero e Linux. Per consultarli puoi (ovviamente) accedere alle relative pagine web o usare un <a href="https://it.wikipedia.org/wiki/Aggregatore">aggregatore</a>, per ricevere automaticamente tutti gli aggiornamenti.
			</p>
			<ul>
				<li>
					<a href="/ils">Planet ILS</a>: News dai soci di Italian Linux Society
				</li>
				<li>
					<a href="/lug">Planet LUG</a>: News dai Linux Users Groups italiani
				</li>
			</ul>
			<p>
				Se preferisci, ti segnaliamo anche la lista <a href="https://twitter.com/LugMap/lists/news-dai-lug">"News dai LUG" su Twitter</a> che include gli accounts di tutti i gruppi indicizzati sulla <a href="http://lugmap.linux.it/">LugMap</a>.
			</p>
			<p>
				Per comunicazioni, segnalazioni, consigli e critiche fai riferimento alla <a href="/contatti">pagina dei contatti</a>.
			</p>
		</div>
	</div>
</div>

<div style="clear: both; margin-bottom: 20px"></div>

<?php lugfooter (); ?>

